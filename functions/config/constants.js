const constants = (function() {

    return {
        apiKeyHeaders: [
            "x-ibm-client-id",
            "x-client-id",
            "X-API-Key"
        ],
        authorizationHeader: 'authorization',
        authorizationTokenPrefix: 'Bearer ',
        oauthTokenPrefix: 'AT',
        JWT_EXPIRATION_TIME: '10y',
        JWT_ISSUER: 'CAMS',
        JWT_AUTH_SECRET_KEY_SSM_PREFIX: '/cams2/auth/jwt/secretkey/dev',
        JWT_AUTH_ISSUER_SSM_PREFIX: '/cams2/auth/jwt/issuer/',
        JWT_AUTH_AUDIENCE_SSM_PREFIX: '/cams2/auth/jwt/audience/dev',
        JWT_AUTH_AUDIENCE: [
            "cams.client.MyMalnutritionTool",
            "cams.client.mymalnutrition",
            "cams.client.MyAdmin",
            "cams.client.BestVendors",
            "cams.client.attribytes",
            "cams.client.itrade",
            "cams.client.iTrade",
            "cams.client.cdl",
            "cams.client.cgsap",
            "cams.client.jitjatjo",
            "cams.client.reactornet",
            "cams.client.wastenot",
            "cams.client.nudge",
            "cams.client.crothal",
            "cams.client.Nudge",
            "cams.client.Laserfiche",
            "cams.client.laserfische",
            "cams.client.laserfiche",
            "cams.client.OneMarket.CTG",
            "cams.client.OneMarket.Avanti",
            "cams.client.OneMarket.ViaTouch",
            "cams.client.OneMarket.365",
            "cams.client.OneMarket.Volante",
            "cams.client.MyReports",
            "cams.client.CDLBE",
            "cams.client.OneMarket.VendorExchange"
        ]
    };

})();

module.exports = constants;

