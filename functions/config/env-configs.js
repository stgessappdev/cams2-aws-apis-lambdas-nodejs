const envConfigs = (function() {
    switch(process.env.ENVIRONMENT){
        case 'prod':
            return {
                urls: {
                    sso: 'https://sso.compassmanager.com'
                },
                JWT_AUTH_SECRET_KEY: 'R2jwoXZzULk8XTLvm6TNfFtEAO1cRdRgylQFM76wtjgZY8lVz4vKVbKB9_od1lTtawnqrYI9W-1lphYrxSyumH2YbuJtjlHHqQxhHsZkrWyfIdy70yJq7YxSJojbFPk30I5nsJnbuI3Nqzs8kgejjd2Qg4yY4A1MBDbarEe-qf-lOOrcbvtbOb7scpX-retTzqM5MOvKrCmY0lH9kW_tTSmY1gRW1cxY2p6TosAhQ-UdWPxliCHWXbB6DMkwn35tdzIsMfZgiCOzLDz3-VRhlnRdHNi1VHEER_9lYP-cWmF5vRVBXJaP01D7eDgD0_AJ6-bAnuJb-qlnTGXn41sJcg'
            };
        case 'qas':
            return {
                urls: {
                    sso: 'https://ssoqas.compassmanager.com'
                },
                JWT_AUTH_SECRET_KEY: 'Cnig7gpD3p8zaE56kAWnzffFEu1Qv7gccxZX_SU18WOyFKIqg_ohIntj0nOQOyMSzUBISs4yBWUtYBJGOdz53zau-kXiWF40mbQ9qZXVh7B86QulP5-2DEu9eAbqjfvGDCK9EiN5Fu09BpHzsymAEct8WE9Mz23Fq82znnUJ7GuCJfIkA6DXd1bJHrTf2ObvjPcqva9NyKq2yuiUAtZt5D5Oi_YmJ7s7N7S_rrpRA004fqFupYgWEAMykLpnWHPB8fFYC2JHathiBedDGO7PdhsKXNLsbLItZepZJJIMIcErcpm201xc1DVW7p3ZW_tn5rxUWjqpB5Uy_Nc6qgGk4A'
            };
        case 'dev':
            return {
                urls: {
                    sso: 'https://ssodev.compassmanager.com'
                },
                JWT_AUTH_SECRET_KEY: 'Cnig7gpD3p8zaE56kAWnzffFEu1Qv7gccxZX_SU18WOyFKIqg_ohIntj0nOQOyMSzUBISs4yBWUtYBJGOdz53zau-kXiWF40mbQ9qZXVh7B86QulP5-2DEu9eAbqjfvGDCK9EiN5Fu09BpHzsymAEct8WE9Mz23Fq82znnUJ7GuCJfIkA6DXd1bJHrTf2ObvjPcqva9NyKq2yuiUAtZt5D5Oi_YmJ7s7N7S_rrpRA004fqFupYgWEAMykLpnWHPB8fFYC2JHathiBedDGO7PdhsKXNLsbLItZepZJJIMIcErcpm201xc1DVW7p3ZW_tn5rxUWjqpB5Uy_Nc6qgGk4A'
            };
        default:
            return {
                urls: {
                    sso: 'https://ssodev.compassmanager.com'
                },
                JWT_AUTH_SECRET_KEY: 'Cnig7gpD3p8zaE56kAWnzffFEu1Qv7gccxZX_SU18WOyFKIqg_ohIntj0nOQOyMSzUBISs4yBWUtYBJGOdz53zau-kXiWF40mbQ9qZXVh7B86QulP5-2DEu9eAbqjfvGDCK9EiN5Fu09BpHzsymAEct8WE9Mz23Fq82znnUJ7GuCJfIkA6DXd1bJHrTf2ObvjPcqva9NyKq2yuiUAtZt5D5Oi_YmJ7s7N7S_rrpRA004fqFupYgWEAMykLpnWHPB8fFYC2JHathiBedDGO7PdhsKXNLsbLItZepZJJIMIcErcpm201xc1DVW7p3ZW_tn5rxUWjqpB5Uy_Nc6qgGk4A'
            };
    }
})();

module.exports = envConfigs;
