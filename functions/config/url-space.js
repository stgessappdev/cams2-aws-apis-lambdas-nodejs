const urlSpace = (function() {

    return {
        sso: {
            tokenValidation: '/oauth2.0/profile?access_token={token}'
        }
    };

})();

module.exports = urlSpace;

