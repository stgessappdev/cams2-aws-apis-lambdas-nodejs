const authenticationService = require("./services/authentication-service");
const policyService = require("./services/policy-service");

/**
  * ClientId Authenticator function.
  * @method authorize
  * @param {String} event.x-ibm-client-id
  * @throws Returns 401 if the clientId is invalid.
  */
exports.lambdaHandler = async (event, context) => {

    console.log("Received EVENT:: ", event);
    
    // Get the API key from the header
    const apiKey = authenticationService.getApiKeyFromEvent(event);

    // Apply principal, generate and return policy
    const policy = policyService.generatePolicy(event, apiKey);

    return policy;
};
