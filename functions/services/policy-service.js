const policyService = (function() {

    let policy = {
        principalId: "",
        usageIdentifierKey: "",
        policyDocument: {
            Version: "2012-10-17",
            Statement: [
                {
                    Action: "execute-api:Invoke",
                    Effect: "Allow",
                    Resource: ""
                }
            ]
        },
        context: {
            simpleAuth: true
        }
    };

    function generatePolicy(event, apiKey) {
        console.log("Generating policy...");
        console.log("Event methodArn: " +event.methodArn);
        if (!apiKey || !event.methodArn) return null;

        let arnParts = event.methodArn.split('/');
        let apiArn = arnParts[0] + '/' + arnParts[1] + '*';

        policy.principalId = apiKey;
        policy.usageIdentifierKey = apiKey;
        policy.policyDocument.Statement[0].Resource = apiArn;

        console.log("Generated the following policy object", JSON.stringify(policy));

        return policy;
    }

    return {
        generatePolicy : generatePolicy
    };

})();

module.exports = policyService;

