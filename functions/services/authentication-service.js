const constants = require("../config/constants");

const authenticationService = (function() {

    /**
     *
     * Get the API Key from the Request Header
     *
     **/
    function getApiKeyFromEvent(event) {

        let apiKey = '';

        for (let header in event.headers) {
            if (constants.apiKeyHeaders.includes(header.toLowerCase())) {
                apiKey = event.headers[header];
                console.log("Found api key header " + header + " with key value of " + apiKey);
            }
        }

        return apiKey;
    }

    /**
     *
     * Get the Authorization Token from the Request Header
     *
     **/
    function getAuthorizationTokenFromEvent(event) {

        let token = '';

        for (let header in event.headers) {
            if (header.toLowerCase() == constants.authorizationHeader && event.headers[header].startsWith(constants.authorizationTokenPrefix)) {
                token = event.headers[header].replace(constants.authorizationTokenPrefix, "");
                console.log("Authorization header value " + token);
            }
        }

        return token;
    }

    // TODO: Get the Secrets from SSM
    function getParameterValueFromSSM(paramName) {
        console.log('In the getParameterFromSystemManager function');
        // Fetches parameter called from SSM parameter store.
        // Requires a policy for SSM:GetParameter on the parameter being read.
        var params = {
            Name: paramName, /* required */
            WithDecryption: true
        };
    }

    return {
        getApiKeyFromEvent : getApiKeyFromEvent,
        getAuthorizationTokenFromEvent: getAuthorizationTokenFromEvent
        // getParameterValueFromSSM : getParameterValueFromSSM
    };

})();

module.exports = authenticationService;
