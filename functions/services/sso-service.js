const https = require("https");

const envConfigs = require("../config/env-configs");
const urlSpace = require("../config/url-space");

const ssoService = (function() {

    async function validateOauthToken(token) {

        console.log("Attempting to call SSO service to validate OAuth token", token);

        const url = envConfigs.urls.sso + urlSpace.sso.tokenValidation.replace("{token}", token);

        console.log("SSO URL :: ", url);

        const promise = new Promise(function(resolve, reject) {

            https.get(url, (res) => {

                let data = "";

                res.on('data', (d) => {
                    data += d;
                });

                res.on("end", () => {

                    if (200 === res.statusCode) {
                        console.error("Response Body: " + data);
                        let profile = JSON.parse(data);
                        resolve(profile);
                    } else {
                        console.error("Error validating OAuth Token, server return status code " + res.statusCode);
                        console.error("Response Body  " + data);
                        reject("Unauthorized");
                    }
                });

            }).on('error', (e) => {
                console.error(e);
                reject("Unauthorized");
            });

        });

        return promise;
    }

    return {
        validateOauthToken : validateOauthToken
    };

})();

module.exports = ssoService;

