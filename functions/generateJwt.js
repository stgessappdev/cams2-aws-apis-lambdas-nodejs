const JWT = require('jsonwebtoken');
const AWS = require('aws-sdk');
const SSM = new AWS.SSM();

const constants = require("./config/constants");
const envConfigs = require("./config/env-configs");
//const authenticationService = require("./services/authentication-service");
/**
 * GET /generate-jwt
 *
 * Returns a JWT, given an aud claims.
 * @method lambdaHandler
 * @param {String} event.header.aud-claim
 * @returns {Object} JWT token
 */
exports.lambdaHandler = (event, context, callback) => {
    console.log("Received EVENT ::: ", event);
    try {
        if (event.headers.Audience) {
            console.log(event.headers.Audience);

            // TODO: Call authentication service
            // const JWT_SECRET_KEY = authenticationService.getParameterValueFromSystemManager(constants.JWT_AUTH_SECRET_KEY_PREFIX);
            // TODO: Get the Secret key from the SSM Parameter Store
            // const JWT_SECRET_KEY = getCamsSsmParam(constants.JWT_AUTH_SECRET_KEY_SSM_PREFIX);
            
            const JWT_SECRET_KEY = envConfigs.JWT_AUTH_SECRET_KEY;
            // Issue JWT
            const token = JWT.sign({
                aud: event.headers.Audience,
                iss: constants.JWT_ISSUER
            }, Buffer.from(JWT_SECRET_KEY, 'base64'), {algorithm: 'HS256', expiresIn: constants.JWT_EXPIRATION_TIME});

            console.log(`JWT issued: ${token}`);
            const response = { // Success response
                token: token
            };

            // Return success response
            console.log(response);
            callback(null, response);
        }
        else {
            console.log(`Error generating JWT`);
            const response = { // Error response
                error: 'Invalid JWT Claims'
            };
            // Return error response
            callback(null, response);
        }
    } catch (e) {
        console.log(`Error generating JWT: ${e.message}`);
        const response = { // Error response
            error: e.message
        };
        // Return error response
        callback(null, response);
    }
};
