const https = require('https');
const jwt = require('jsonwebtoken');

const envConfigs = require("./config/env-configs");
const urlSpace = require("./config/url-space");
const constants = require("./config/constants");

//const ssoService = require("./services/sso-service");
//const authenticationService = require("./services/authentication-service");

/**
 * JWT Authorizer function.
 * @method authorize
 * @param {String} event.authorizationToken - JWT
 * @throws Returns 401 if the token is invalid or has expired.
 * @throws Returns 403 if the token does not have sufficient permissions.
 */
exports.lambdaHandler = async (event, context) => {
    console.log("===================");
    console.log("EVENT :: ", event);
    console.log("===================");
    
    if (event.headers.Authorization) {
        // Get the API key from the header
        let token = event.headers.Authorization.replace(constants.authorizationTokenPrefix, "");
        console.log("Authorization header value " + token);
        
        if (token.startsWith(constants.oauthTokenPrefix)) { // This is an OMS token
            // const ssoPromise = ssoService.validateOAuthToken(token);
            const url = envConfigs.urls.sso + urlSpace.sso.tokenValidation.replace("{token}", token);
            console.log("SSO URL:", url);
        
            const ssoPromise = new Promise(function(resolve, reject) {
                https.get(url, (res) => {
                    let data = "";
                    res.on('data', (d) => {
                        data += d;
                    });
                    res.on("end", () => {
                        if (200 === res.statusCode) {
                            console.log("Success Response Body: " + data);
                            let profile = JSON.parse(data);
                            resolve(profile);
                        } else {
                            console.error("Error validating OAuth Token, server return status code " + res.statusCode);
                            console.error("Failure Response Body " + data);
                            reject('Unauthorized');
                        }
                    });
                }).on('error', (e) => {
                    console.error(e);
                    reject("Unauthorized");
                });
            });
            return ssoPromise;
        } else { // This is a JWT token
            // const jwtPromise = authenticationService.validateJWTToken(token);
            const jwtPromise = new Promise(function(resolve, reject) {
                // Verify JWT - Here we validate that the JWT is valid and has been created using the same secret pass phrase, issuer and audience claims
                jwt.verify(token, Buffer.from(envConfigs.JWT_AUTH_SECRET_KEY, 'base64'),{ issuer: constants.JWT_ISSUER, audience: constants.JWT_AUTH_AUDIENCE, algorithm: "HS256", ignoreExpiration: true },(err, decoded) => {
                    // If there is an error..
                    if (err) {
                        console.error(`Error Verifying Token: ${err}`);
                        // Return error response
                        reject('Unauthorized');
                    }
                    else {
                        // If the JWT is valid
                        const response = {
                            // Success response
                            id: decoded.aud
                        };
                        console.log(response);
                        // Return success response
                        resolve(response);
                    }
                });
            });
            return jwtPromise;
        }
    }
    else {
        return Promise.reject('Unauthorized');
    }
}
